# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

## [Unreleased]

## [0.3.2] - 2020-12-03
### Fixed
- GitLab CI/CD.

## [0.3.1] - 2020-12-03
### Fixed
- Do not show templates.
- Remove special JOSM case.
- Heading.

## [0.3.0][] - 2019-06-22
### Added
- Gitlab CI/CD config file.

### Changed
- Redesign webpage.

## [0.2.2][] - 2019-06-19
### Changed
- Rewrite onload in index file.

## [0.2.1][] - 2019-05-24
### Changed
- Registration link as other links.

## [0.2.0][] - 2019-05-19
### Added
- NGINX example config.
- Link to prijdu.mapathon.cz registration for JOSM mapathons.

## [0.1.1][] - 2019-03-22
### Fixed
- Link to license in readme.

## 0.1.0 - 2019-03-22
### Added
- Changelog, license, readme.
- Initial version of web page with the list of planned mapathons in CZ & SK.

[Unreleased]: https://gitlab.com/qeef/bude.mapathon.cz/compare/v0.3.2...master
[0.3.2]: https://gitlab.com/qeef/bude.mapathon.cz/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/qeef/bude.mapathon.cz/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/qeef/bude.mapathon.cz/compare/v0.2.2...v0.3.0
[0.2.2]: https://gitlab.com/qeef/bude.mapathon.cz/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/qeef/bude.mapathon.cz/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/qeef/bude.mapathon.cz/compare/v0.1.1...v0.2.0
[0.1.1]: https://gitlab.com/qeef/bude.mapathon.cz/compare/v0.1.0...v0.1.1
