# bude.mapathon.cz
Web page with the list of planned mapathons in CZ & SK.

# NGINX config
In file `/etc/nginx/sites-available/bude.mapathon.cz`:
```
server {
        listen 80;
        listen [::]:80;

        server_name bude.mapathon.cz;

        root /var/www/bude.mapathon.cz;
        index index.html;

        location / {
                try_files $uri $uri/ =404;
        }
}
```

# Contribute
## Code
For quick orientation see the [changelog][].

Please, think about [The seven rules of a great Git commit message][] when
making commit. The project use [OneFlow][] branching model with the `master`
branch as the branch where the development happens.

## License
This project is developed under [GNU AGPLv3 license][].

[changelog]: ./CHANGELOG.md
[The seven rules of a great Git commit message]: https://chris.beams.io/posts/git-commit/
[OneFlow]: http://endoflineblog.com/oneflow-a-git-branching-model-and-workflow
[GNU AGPLv3 license]: ./LICENSE
